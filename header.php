<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Becker
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:200,300,400,500,600,700" rel="stylesheet">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/safari-pinned-tab.svg" color="#f37321">
	<meta name="msapplication-TileColor" content="#ffc40d">
	<meta name="theme-color" content="#ffffff">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/ncz1guv.css">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'becker' ); ?></a>

	<header id="masthead" class="site-header">
		<?php
		if ( is_home() || is_singular('post') || is_singular('issue') ) {
			global $post; ?>
			<div id="post-nav">
				<h6><a href="<?php echo get_permalink( get_option('page_for_posts') ); ?>">BLOG</a></h6>
				<?php
				if ( is_singular('post') && $issue = get_field('issue_x_post', $post->ID) ) {
					echo file_get_contents(get_template_directory_uri() . '/images/ui/arrow.svg'); ?>
					<a class="uppercase grotesque weight-600 black" href="<?php echo get_permalink($issue[0]); ?>"><?php echo get_the_title($issue[0]); ?></a>
				<?php
				} ?>
			</div>
		<?php
		}
		if ( !is_home() && !is_front_page() && !is_single() ) { ?>
			<a href="#" class="becker-back-button">
				<?php echo file_get_contents(get_template_directory_uri() . '/images/ui/arrow.svg'); ?>
			</a>
		<?php
		} ?>
		<div id="header-content">
			<div class="site-branding">
				<a class="site-logo flex justify-center align-center orange-bg" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg"/>
				</a>
			</div><!-- .site-branding -->

			<div class="nav-toggle flex justify-center align-center">
				<button class="hamburger hamburger--emphatic" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
			</div>
		</div>

		<div id="site-navigation-container">
			<nav id="site-navigation" class="main-navigation flex justify-center align-center">
				<div class="content">
					<?php
					// $menu_items = wp_get_nav_menu_items( 'main-menu' );
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</div>
			</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->

	<?php
	if ( !is_singular('post') && !is_front_page() && !is_home() ) { ?>
		<h6 class="page-label uppercase weight-900"><span class="page-label-inner-wrapper"><span class="page-label-inner-inner"><?php echo the_title(); ?></span></span></h6>
	<?php
	}
	if ( is_home() || is_singular('post') ) { ?>
		<h6 class="page-label uppercase weight-900">Blog</h6>
	<?php
	} ?>

	<div id="content" class="site-content">
