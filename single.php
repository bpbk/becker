<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post(); ?>
			<header id="blog-header" class="bg-centered <?php echo get_field('graphical_featured_image') ? 'article-graphic' : 'article-image'; ?>" style="background-image:url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large'); ?>)">
			</header>
			<div class="content">
				<div class="row">
					<div class="col-10 post-title-wrapper">
						<div id="post-title">
							<h1 class="uppercase font-7"><?php the_title(); ?></h1>
							<h2 class="uppercase font-7"><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
				<div id="post-content">
					<div class="row align-right">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		<?php

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
