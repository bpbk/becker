<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header orange-bg">
					<div class="content">
						<h1 class="uppercase font-6 mt-2"><?php esc_html_e( 'We can do lots of things,', 'becker' ); ?></h1>
						<h2 class="uppercase font-4"><?php esc_html_e( 'but it looks like we can’t do that.', 'becker' ); ?></h2>
					</div>
				</header><!-- .page-header -->

				<div class="page-content">
					<div class="content">
						<div class="button-404-wrapper">
							<div class="button-404-container">
								<a class="font-2 grotesque-cond weight-600 black uppercase py-1 px-3 button-404" href="<?php echo home_url(); ?>">Go back home</a>
							</div>
						</div>
					</div>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
