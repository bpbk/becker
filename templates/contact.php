<?php
/**
 * Template Name:  Contact
 *
 * The template for displaying the contact page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			if ( $audit_form = get_field('contact_header') ) { ?>
				<section id="contact-form-section" class="becker-form">
					<div class="header-bg row">
						<div class="col-6 orange-bg">

						</div>
						<div class="col-6 orange-bg">

						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-6 white form-left black pt-5 pb-3">
								<?php
								if ( $form_left = $audit_form['form_left_side'] ) {
									echo $form_left;
								} ?>
								<div class="phone">
									<i class="fas fa-phone"></i> <span><a href="tel:5104656056">510 465 6200 x 106</a></span>
								</div>
							</div>
							<div class="col-6 pb-3 pt-2">
								<?php
								if ( $form_title = $audit_form['title'] ) { ?>
									<div class="form-title-container">
										<h3 class="form-title uppercase grotesque-extra-cond"><?php echo $form_title; ?></h3>
									</div>
								<?php
								}
								if ( $form_right = $audit_form['form_right_side'] ) {
									echo $form_right;
								} ?>
							</div>
						</div>
					</div>
				</section>
			<?php
			}
			if ( $locations = get_field('becker_locations', 'option') ) { ?>
				<section id="becker-locations-page">
					<div class="content">
						<div class="row">
							<?php
							foreach ( $locations as $location ) { ?>
								<div class="becker-location-container col-6 my-2<?php echo $location['featured'] ? ' featured' : ''; ?>">
									<div class="becker-location pt-2 px-3">
										<?php
										if ( $location['featured'] ) { ?>
											<div class="featured-location"></div>
										<?php
										} ?>
										<h2 class="uppercase grotesque-extra-cond black font-5"><?php echo $location['location']; ?></h2>
										<div class="becker-location-info font-1 weight-light grotesque">
											<?php
											if ( $location['address'] ) {
												echo $location['address'];
											}
											if ( $location['phone'] ) { ?>
												<p><?php echo $location['phone']; ?></p>
											<?php
											}
											if ( $location['email'] ) { ?>
												<a href="mailto:<?php echo $location['email']; ?>"><?php echo $location['email']; ?></a>
											<?php
											} ?>
										</div>
									</div>
								</div>
							<?php
							} ?>
						</div>
					</div>
				</section>
			<?php
			}
			becker_testimonials();

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
