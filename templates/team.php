<?php
/**
 * Template Name:  Team
 *
 * The template for displaying the team page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<section id="our-leaders" class="py-2">
				<div id="our-leaders-container" class="content">
					<h2 class="leaders-header uppercase">Our Leaders</h2>
					<div class="row">
						<?php
						if( $leaders = get_field('leaders', 'option') ):
							foreach ( $leaders as $leader ) {
								$leader_name = $leader['name'];
								$leader_name = explode(' ', $leader_name, 2); ?>
								<div class="leader col-4 animate" data-animate="fadeInUp">
									<div class="leader-image-wrapper">
										<div class="leader-image">
											<a href="<?php echo home_url() . '/team-bios/#' . slugify($leader['name']); ?>">
												<img src="<?php echo $leader['image']['sizes']['medium']; ?>"/>
											</a>
										</div>
										<div class="leader-info">
											<h3 class="leader-name uppercase">
												<a href="<?php echo home_url() . '/team-bios/#' . slugify($leader['name']); ?>">
													<?php echo $leader_name[0]; ?>
													<span><?php echo $leader_name[1]; ?></span>
												</a>
											</h3>
											<?php
											if ( $leader['title'] ) { ?>
												<p><?php echo $leader['title']; ?></p>
											<?php
											}
											if ( $leader['email'] ) { ?>
												<a href="email:<?php echo $leader['email']; ?>"><?php echo $leader['email']; ?></p>
											<?php
											}
											if ( $leader['phone'] ) { ?>
												<a class="becker-tel" href="tel:<?php echo $leader['phone']; ?>"><?php echo $leader['phone']; ?></a>
											<?php
											}
											?>
										</div>
									</div>
								</div>
							<?php
							}
						endif; ?>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
