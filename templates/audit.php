<?php
/**
 * Template Name:  Audit
 *
 * The template for displaying the about page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			if ( $audit_form = get_field('audit_header') ) { ?>
				<section id="audit-form-section" class="becker-form">
					<div class="header-bg row">
						<div class="col-6 orange-bg">

						</div>
						<div class="col-6 orange-bg">

						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-6 white form-left black pt-5 pb-3">
								<img alt="Get 50% to 200% life in lead flow for the same spend. *average result" src="<?php echo get_template_directory_uri(); ?>/images/graphics/lift-lead-flow.svg"/>
							</div>
							<div class="col-6 pb-3 pt-5">
								<?php
								if ( $form_title = $audit_form['title'] ) { ?>
									<div class="form-title-container">
										<h3 class="form-title uppercase grotesque-extra-cond"><?php echo $form_title; ?></h3>
									</div>
								<?php
								}
								if ( $form_right = $audit_form['form_right_side'] ) {
									echo $form_right;
								} ?>
							</div>
						</div>
					</div>
				</section>
			<?php
			}
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
