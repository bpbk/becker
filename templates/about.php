<?php
/**
 * Template Name:  About
 *
 * The template for displaying the about page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post(); ?>
      <header class="fifty-fifty orange-bg">
        <div class="content">
          <div class="row">
            <?php
            if ( $header_left = get_field('header_left') ) { ?>
              <div class="header-left col-6 pr-1 pt-3 pb-5">
                <h1 class="uppercase"><?php echo $header_left; ?></h1>
              </div>
            <?php
            }
            if ( $header_right = get_field('header_right') ) { ?>
              <div class="header-right col-6 flex justify-center align-center text-centered pt-3 pb-5">
								<div>
									<img src="<?php echo get_template_directory_uri(); ?>/images/graphics/brackets.svg" alt="logo brackets"/>
									<h2 class="grotesque-extra-cond font-5 font-light uppercase"><?php echo $header_right; ?></h2>
								</div>
              </div>
            <?php
            } ?>
          </div>
        </div>
      </header>
      <section id="about-content">
        <div class="content">
          <div class="row">
            <div class="col-4 stretch">
							<img src="<?php echo get_template_directory_uri(); ?>/images/graphics/number-one.svg"/>
            </div>
            <div class="col-8 stretch">
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
