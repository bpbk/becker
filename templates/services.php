<?php
/**
 * Template Name:  Services
 *
 * The template for displaying the services page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			$args = [
				'post_type' => 'service',
				'posts_per_page' => -1
			];
			$services_query = new WP_Query( $args ); ?>
      <header class="page-header orange-bg">
        <div class="content">
          <h1 class="font-9 acumin uppercase">Services</h1>
        </div>
      </header>
      <section id="services-content">
        <div class="content align-left">
          <div class="row">
            <div id="services-menu-container" class="col-3">
							<div id="services-menu">
								<!--<h3 class="font-2 mb-1">Services</h3>-->
								<?php
								if( $services = get_field('services', 'option') ) {
									foreach ( $services as $service ) { ?>
										<a class="uppercase primary-font service-link service-link-<?php echo slugify($service['title']); ?>" href="#<?php echo slugify($service['title']); ?>">
											<?php echo $service['title']; ?>
										</a>
									<?php
									}
								} ?>
							</div>
            </div>
            <div id="services" class="col-9">
							<?php
							if( $services = get_field('services', 'option') ):
								foreach ( $services as $service ) { ?>
									<a style="display:block;" name="<?php echo slugify($service['title']); ?>"></a>
									<section class="service service-<?php echo slugify($service['title']); ?>" data-title="<?php echo slugify($service['title']); ?>">
										<h2 class="acumin font-7 uppercase"><?php echo $service['title']; ?></h2>
										<div class="row">
											<?php
											if ( $service['description'] ) { ?>
												<div class="col-7">
													<h3 class="orange acumin services-desc no-transform font-4"><?php echo $service['description']; ?></h3>
												</div>
											<?php
											}
											if ( $service['details'] ) { ?>
												<div class="col-5">
													<?php
													foreach ( $service['details'] as $detail ) { ?>
														<div class="service-detail flex align-center">
															<div class="detail-checkbox">
																<?php becker_checkbox(); ?>
															</div>
															<div class="detail-content">
																<?php echo $detail['detail']; ?>
															</div>
														</div>
													<?php
													}
													if ( $badge = $service['badge'] ) { ?>
														<div class="service-badge">
															<?php echo apply_filters('the_content', $badge); ?>
														</div>
													<?php
													} ?>
												</div>
											<?php
											} ?>
										</div>
									</section>
								<?php
								}
							endif; ?>
            </div>
          </div>
        </div>
      </section>
		<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
