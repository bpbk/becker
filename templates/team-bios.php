<?php
/**
 * Template Name:  Team Bios
 *
 * The template for displaying the team page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div id="leaders-slider-module">
				<div id="leaders-slider-container">
					<div class="header-bg row">
						<div class="col-6 orange-bg">

						</div>
						<div class="col-6 white-bg"></div>
					</div>
					<?php
					// if( $leaders = get_field('leaders', 'option') ):
					// 	foreach ( $leaders as $leader ) {
					// 	print_r($leader['svg']);
					// 	}
					// endif; ?>
					<div id="leaders-slider">
						<?php
						if( $leaders = get_field('leaders', 'option') ):
							foreach ( $leaders as $leader ) {
								$leader_name = $leader['name'];
								$leader_name = explode(' ', $leader_name, 2); ?>
								<div
									class="leader-slide"
									id="<?php echo slugify($leader['name']); ?>"
									data-slug="<?php echo slugify($leader['name']); ?>"
									data-name="<?php echo $leader['name']; ?>"
									data-title="<?php echo $leader['title']; ?>"
									data-image="<?php echo $leader['image']['sizes']['large']; ?>">
									<div class="header-bg row">
										<div class="col-6">

										</div>
										<?php
										if ( $image = $leader['image'] ) { ?>
											<div class="col-6 header-bg-image">
												<?php
												if ( $leader['image']['mime_type'] == 'image/svg+xml' ) { ?>
													<div class="becker-loader flex justify-center align-center">
														<?php echo file_get_contents(get_template_directory_uri() . '/images/puff-1.svg'); ?>
													</div>
												<?php
												} else { ?>
													<img class="header-bg-leader-image" src="<?php echo $leader['image']['sizes']['small']; ?>"/>
												<?php
												} ?>
											</div>
										<?php
										} ?>
									</div>
									<div class="leader-header-content">
										<div class="content leader-content">
											<h6 class="leader-label uppercase">Leader</h6>
											<div class="row leader-header-title">
												<div class="col-7">
													<h1 class="font-9 mb-1 mt-0 uppercase smaller-font">
														<?php echo $leader_name[0]; ?>
														<span><?php echo $leader_name[1]; ?></span>
													</h1>
												</div>
											</div>
											<div class="row leader-bio">
												<div class="col-6">
													<div class="leader-details my-2">
														<?php
														if ( $title = $leader['title'] ) { ?>
															<div class="leader-detail">
																<img src="<?php echo get_template_directory_uri(); ?>/images/ui/person.svg"/>
																<p class="my-0"><?php echo $title; ?></p>
															</div>
														<?php
														}
														if ( $email = $leader['email'] ) { ?>
															<div class="leader-detail">
																<img src="<?php echo get_template_directory_uri(); ?>/images/ui/email.svg"/>
																<p class="my-0"><?php echo $email; ?></p>
															</div>
														<?php
														}
														if ( $phone = $leader['phone'] ) { ?>
															<div class="leader-detail">
																<img src="<?php echo get_template_directory_uri(); ?>/images/ui/phone.png"/>
																<p class="my-0"><?php echo $phone; ?></p>
															</div>
														<?php
														} ?>
													</div>
													<div class="leader-desc">
														<?php echo $leader['description']; ?>
													</div>
												</div>
											</div>
										</div>
										<div class="content">
											<div class="row">
												<div class="scroll-indicator col-6">
													<?php echo file_get_contents(get_template_directory_uri() . '/images/ui/arrow.svg'); ?>
												</div>
											</div>
										</div>
									</div>

								</div>
							<?php
							}
						endif; ?>
					</div>
				</div>
				<div class="leaders-nav-container">
					<div id="leaders-nav">

					</div>
						<!-- <div class="leaders-nav-scroll-indicator">
							<a class="nav-scroll" href="#navigation"></a>
						</div> -->
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
