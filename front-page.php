<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<header id="home-header" class="orange-bg">
				<div class="content">
					<div id="slogan-container" class="animate children" data-animate="slamLeft">
						<h2 class="slogan uppercase">
							More</br>Students
						</h2>
						<h2 class="uppercase delay-1s">
							Less</br>Dollars
						</h2>
					</div>
					<div class="scroll-indicator">
						<div class="scroll-indicator-button">
							<?php echo file_get_contents(get_template_directory_uri() . '/images/ui/arrow.svg'); ?>
						</div>
					</div>
				</div>
			</header>
			<section id="what-we-do" class="orange-bg">
				<div class="content">
					<div class="row">
						<div id="services-home-nav-container" class="col-6">
							<div id="services-mobile" class="animate" data-animate="fadeInLeft">
								<h2 class="uppercase font-4 mb-2 acumin weight-900">
									<a style="color: #080505;" href="<?php echo home_url('services'); ?>">Services</a>
								</h2>
								<?php
								if( $services = get_field('services', 'option') ):
	                foreach ( $services as $key => $service ) { ?>
										<div class="service-container<?php echo $key == 0 ? ' is-active' : ''; ?>">
											<div class="service-label">
												<span><?php echo $service['title']; ?></span>
											</div>
											<div class="services-slide-content">
												<?php echo $service['description']; ?>
												<a class="service-link" href="<?php echo home_url('services'); ?>#<?php echo slugify($service['title']); ?>"></a>
											</div>
										</div>
									<?php
									}
								endif; ?>
							</div>
							<div id="services-home-nav" class="animate" data-animate="fadeInLeft">
								<h2 class="uppercase font-4 mb-2 acumin weight-900">
									<a style="color: #080505;" href="<?php echo home_url('services'); ?>">Services</a>
								</h2>
							</div>
						</div>
						<div id="services-info-section" class="col-6">
							<div id="services-slider" class="animate" data-animate="fadeInDown">
								<?php
	              if( $services = get_field('services', 'option') ):
	                foreach ( $services as $service ) { ?>
	                  <div
											class="services-slide flex align-center"
											data-title="<?php echo $service['title']; ?>"
											data-excerpt="<?php echo $service['description']; ?>"
											data-slug="<?php echo slugify($service['title']); ?>">
	                    <div class="services-slide-content">
	                    	<?php echo $service['description']; ?>
												<a class="service-link" href="<?php echo home_url('services'); ?>#<?php echo slugify($service['title']); ?>"></a>
	                    </div>
	                  </div>
	                <?php
									}
	              endif; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php becker_divider('more students', 'less dollars'); ?>
			<section id="our-leaders" class="py-2">
				<div id="our-leaders-container" class="content">
					<h2 class="leaders-header uppercase">Our Leaders</h2>
					<div class="row">
						<?php
						if( $leaders = get_field('leaders', 'option') ):
							foreach ( $leaders as $leader ) {
								$leader_name = $leader['name'];
								$leader_name = explode(' ', $leader_name, 2); ?>
								<div class="leader col-4 animate" data-animate="fadeInUp">
									<div class="leader-image-wrapper">
										<div class="leader-image">
											<a href="<?php echo home_url() . '/team-bios/#' . slugify($leader['name']); ?>">
												<img src="<?php echo $leader['image']['sizes']['medium']; ?>"/>
											</a>
										</div>
										<div class="leader-info">
											<h3 class="leader-name uppercase">
												<a href="<?php echo home_url() . '/team-bios/#' . slugify($leader['name']); ?>">
													<?php echo $leader_name[0]; ?>
													<span><?php echo $leader_name[1]; ?></span>
												</a>
											</h3>
											<?php
											if ( $leader['title'] ) { ?>
												<p><?php echo $leader['title']; ?></p>
											<?php
											}
											if ( $leader['email'] ) { ?>
												<a href="email:<?php echo $leader['email']; ?>"><?php echo $leader['email']; ?></p>
											<?php
											}
											if ( $leader['phone'] ) { ?>
												<a class="becker-tel" href="tel:<?php echo $leader['phone']; ?>"><?php echo $leader['phone']; ?></a>
											<?php
											}
											?>
										</div>
									</div>
								</div>
							<?php
							}
						endif; ?>
					</div>
				</div>
			</section>
			<?php

			becker_testimonials();

			if ( $audit_form = get_field('audit_form_section', get_option('page_on_front')) ) { ?>
				<section id="home-audit-form-section" class="becker-form cross">
					<div class="header-bg row">
						<div class="col-6 orange-bg">

						</div>
						<div class="col-6 white-bg">

						</div>
					</div>
					<div class="content">
						<div class="row">
							<div class="col-6 white form-left">
								<?php
								if ( $form_left = $audit_form['form_left_side'] ) {
									echo $form_left;
								} ?>
								<!--<img src="<?php echo get_template_directory_uri(); ?>/images/graphics/lift-lead-flow.svg" alt="lift in lead flow"/>-->
							</div>
							<div class="col-6 form">
								<?php
								if ( $form_title = $audit_form['title'] ) { ?>
									<div class="form-title animate" data-animate="jiggle">
										<?php echo file_get_contents(get_template_directory_uri() . '/images/graphics/get_your_audit.svg'); ?>
									</div>
								<?php
								}
								if ( $form_right = $audit_form['form_right_side'] ) {
									echo $form_right;
								} ?>
							</div>
						</div>
					</div>
				</section>
			<?php
			} ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
