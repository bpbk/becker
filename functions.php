<?php
/**
 * Becker functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Becker
 */

if ( ! function_exists( 'becker_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function becker_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Becker, use a find and replace
		 * to change 'becker' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'becker', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'small', 400 );
		add_image_size( 'small-medium', 600 );
		add_image_size( 'medium', 800 );
		add_image_size( 'large', 1400 );
		add_image_size( 'extra-large', 1900 );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'becker' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'becker_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'becker_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function becker_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'becker_content_width', 640 );
}
add_action( 'after_setup_theme', 'becker_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function becker_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'becker' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'becker' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'becker_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function becker_scripts() {
	$manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main;

	wp_enqueue_script('bigtext', get_template_directory_uri() . '/js/bigtext.js', ['jquery'], null, true);

	wp_enqueue_style( 'becker-style', get_template_directory_uri() . $main->css, false, null );

	wp_enqueue_script('becker-js', get_template_directory_uri() . $main->js, ['jquery'], null, true);

	wp_enqueue_script( 'becker-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'becker-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'becker_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom post types
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * Shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';
/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_action('wp_head','pluginname_ajaxurl');
function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
}

add_action('wp_ajax_nopriv_do_ajax', 'ajax_function');
add_action('wp_ajax_do_ajax', 'ajax_function');

function ajax_function(){
  switch($_REQUEST['fn']){
    case 'get_past_issues':
      $paged = $_REQUEST['paged'];
			$issue = $_REQUEST['issue'];
			echo json_encode(get_past_issues($issue, $paged));
	  break;
		case 'get_leader_image':
			$team_index = $_REQUEST['index'];
			$leaders = get_field('leaders', 'option');
			if ( $leaders[$team_index]['image']['mime_type'] == 'image/svg+xml' ) {
				echo file_get_contents($leaders[$team_index]['image']['sizes']['large']);
			}
		break;
		default:
			$output = 'nothing here';
			echo $output;
		break;
	}
	die();
}

function slugify($text)
{
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function becker_divider ($orange, $black) { ?>
	<section class="becker-divider-section">
		<div class="becker-divider animate show">
			<?php
			for ( $i = 1; $i < 6; $i++) { ?>
				<p class="divider-line">
					<?php echo file_get_contents(get_template_directory_uri() . '/images/graphics/msld.svg'); ?>
				</p>
			<?php
			} ?>
		</div>
	</section>
<?php
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Team',
		'menu_title'	=> 'Team',
		'menu_slug' 	=> 'becker-team',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position' 		=> 4,
		'icon_url'		=> 'dashicons-admin-users'
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Services',
		'menu_title'	=> 'Services',
		'menu_slug' 	=> 'becker-services',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'position' 		=> 5,
		'icon_url'		=> 'dashicons-welcome-learn-more'
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Locations',
		'menu_title'	=> 'Locations',
		'menu_slug' 	=> 'becker-locations',
		'capability'	=> 'edit_posts',
		'position' 		=> 6,
		'icon_url'		=> 'dashicons-location-alt'
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Testimonials',
		'menu_title'	=> 'Testimonials',
		'menu_slug' 	=> 'becker-testimonials',
		'capability'	=> 'edit_posts',
		'position' 		=> 7,
		'icon_url'		=> 'dashicons-format-quote'
	));
	//
	// acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Testimonials',
	// 	'menu_title'	=> 'Testimonials',
	// 	'parent_slug'	=> 'nycjw-settings',
	// ));

}

function social_links () { ?>
	<div class="social-links flex">
		<a target="_blank" href="https://www.facebook.com/BeckerSocial/" class="social-link flex justify-center align-center">
			<i class="fab fa-facebook-f"></i>
		</a>
		<a target="_blank" href="https://twitter.com/beckermedia" class="social-link flex justify-center align-center">
			<i class="fab fa-twitter"></i>
		</a>
		<a target="_blank" href="https://www.linkedin.com/company/becker-media" class="social-link flex justify-center align-center">
			<i class="fab fa-linkedin-in"></i>
		</a>
	</div>
<?php
}

function becker_checkbox () { ?>
	<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 29 31" style="enable-background:new 0 0 29 31;" xml:space="preserve">
		<style type="text/css">
			.st0{fill:#F37321;}
		</style>
		<title>Check</title>
		<desc>Created with Sketch.</desc>
		<g id="Design">
			<g id="Design_x2F_Mobile_x5F_Home-Copy-4" transform="translate(-28.000000, -630.000000)">
				<g id="Check" transform="translate(28.000000, 630.000000)">
					<g id="Brackets_Orange">
						<g id="Group">
							<polygon id="Shape" class="st0" points="2.5,7 0.1,7 0.1,0 29,0 29,6.9 26.6,6.9 25.9,2.3 3.2,2.3 					"/>
							<polygon id="Shape_1_" class="st0" points="26.6,23.8 29,23.8 29,30.8 0.1,30.8 0.1,23.9 2.5,23.9 3.2,28.5 25.9,28.5 					"/>
						</g>
					</g>
					<path id="checkbox-check" d="M19.3,12.8l-0.9-0.9c-0.1-0.1-0.3-0.2-0.4-0.2c-0.2,0-0.3,0.1-0.4,0.2l-4.1,4.3l-1.8-1.9
						c-0.1-0.1-0.3-0.2-0.4-0.2c-0.2,0-0.3,0.1-0.4,0.2l-0.9,0.9c-0.1,0.1-0.2,0.3-0.2,0.4c0,0.2,0.1,0.3,0.2,0.4l2.3,2.4l0.9,0.9
						c0.1,0.1,0.3,0.2,0.4,0.2c0.2,0,0.3-0.1,0.4-0.2l0.9-0.9l4.5-4.8c0.1-0.1,0.2-0.3,0.2-0.4C19.4,13.1,19.4,12.9,19.3,12.8z"/>
				</g>
			</g>
		</g>
		</svg>
<?php
}

function my_custom_mime_types( $mimes ) {

// New allowed mime types.
$mimes['svg'] = 'image/svg+xml';
$mimes['svgz'] = 'image/svg+xml';
$mimes['doc'] = 'application/msword';

// Optional. Remove a mime type.
unset( $mimes['exe'] );

return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );

function becker_testimonials() {
	if ( $testimonials = get_field('testimonials', 'option') ) { ?>
		<div id="testimonials-section" class="cross">
			<div id="testimonial-header" class="animate" data-animate="fadeInDown">
				<?php echo file_get_contents(get_template_directory_uri() . '/images/graphics/we_get_results.svg'); ?>
			</div>
				<div class="testimonials-slider-wrapper">
					<div id="testimonials-slider-container" class="animate" data-animate="fadeInUp">
						<div id="testimonials-slider" data-slide-time="<?php echo get_field('test_slide_time', 'option'); ?>" data-fade-time="<?php echo get_field('test_fade_time', 'option'); ?>" data-autoplay="<?php echo get_field('test_autoplay', 'option'); ?>">
							<?php
							foreach ( $testimonials as $testimonial ) { ?>
								<div class="testimonial-slide">
									<div class="content">
										<div class="testimonial-quote">
											<?php echo $testimonial['quote']; ?>
										</div>
										<div class="testimonial-author">
											<?php
											if ( $testimonial['author_image'] ) { ?>
												<div class="testimonial-author-image">
													<img src="<?php echo $testimonial['author_image']['sizes']['small']; ?>"/>
												</div>
											<?php
											}
											if ( $testimonial['author'] ) { ?>
												<span><?php echo $testimonial['author']; ?></span>
											<?php
											} ?>
										</div>
										<?php
										if ( $testimonial['company'] ) { ?>
											<p class="testimonial-company"><strong><?php echo $testimonial['company']; ?></strong></p>
										<?php
										} ?>
									</div>
								</div>
							<?php
							} ?>
						</div>
						<div id="becker-arrows">
							<div class="becker-arrow prev-arrow flex align-center justify-center">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129">
									<g>
										<path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
									</g>
								</svg>
							</div>
							<div class="becker-arrow next-arrow flex align-center justify-center">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129">
									<g>
										<path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
									</g>
								</svg>
							</div>
						</div>
					</div>
				</div>
		</div>
	<?php
	}
}

function past_issues($issue, $label) {
	if ( $past_issues = get_past_issues($issue) ) {
		if ( !$past_issues['issues'] ) {
			return;
		} ?>
		<section id="past-issues-container">
			<div class="content">
				<h6 class="animate" data-animate="fadeInUp"><?php echo $label ? $label : 'Previous Issues'; ?></h6>
				<div class="row past-issues-row">
					<?php
					echo $past_issues['issues']; ?>
				</div>
				<?php
				if ( $past_issues['more_issues'] ) { ?>
					<div class="more-issues-container animate" data-animate="fadeInUp">
						<a class="more-issues uppercase orange-bg white weight-900" href="#" data-issue="<?php echo $issue; ?>">More Issues</a>
					</div>
					<?php
				} ?>
			</div>
		</section>
	<?php
	}
}

function get_past_issues($issue, $paged = 1) {
	$issues = [
		'more_issues' => false,
		'issues' => false
	];
	$args = [
		'post_type' => 'issue',
		'posts_per_page' => 6,
		'paged' => $paged,
		'post__not_in' => [$issue]
	];
	$issues_query = new WP_Query( $args );
	if( $issues_query->have_posts() ):
		ob_start();
		while ( $issues_query->have_posts() ) : $issues_query->the_post(); ?>
			<div class="col-6 animate" data-animate="fadeInUp">
				<div class="past-issue-image-wrapper">
					<div class="past-issue-image-container">
						<div class="past-issue-image-content">
							<a href="<?php echo get_permalink(); ?>" class="past-issue-image bg-centered" style="background-image:url(<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large' ); ?>)">
							</a>
						</div>
					</div>
				</div>

				<p class="past-issue-date font-1 acumin">
					<strong>
						<?php echo get_the_date(); ?>
					</strong>
				</p>

				<a href="<?php echo get_permalink(); ?>" class="past-issue-title font-2 black font-light grotesque mt-0">
					<?php the_title(); ?>
				</a>
			</div>
		<?php
		endwhile;
		wp_reset_postdata();
		$issues['issues'] = ob_get_clean();
		if( $issues_query->max_num_pages > $args['paged'] ) {
      $issues['more_issues'] = true;
    }
	endif;
	return $issues;
}
