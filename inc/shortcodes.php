<?php
function show_additional_links( $atts, $content, $tags ) {
  $a = shortcode_atts( array(), $atts );
  ob_start(); ?>
  <div class="show-more-links">
    <?php echo $content; ?>
  </div>
  <?php
  return ob_get_clean();
}

//add_shortcode( 'additional_links', 'show_additional_links' );
