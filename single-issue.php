<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Becker
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			$issue_ID = get_the_ID(); ?>
			<header id="first-blog-post">
				<div class="header-bg row">
					<div class="col-6 orange-bg">

					</div>
					<?php
					if ( has_post_thumbnail() ) { ?>
						<div class="col-6 header-bg-image">
							<img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'large' ); ?>"/>
						</div>
					<?php
					} ?>
				</div>
				<div class="content">
					<div class="first-post-content">
						<div class="row first-post-title">
							<div class="col-7">
								<h6 class="uppercase">In This Issue</h6>
								<h1 class="font-7 mb-1 mt-0 acumin uppercase"><?php the_title(); ?></h1>
							</div>
						</div>
						<?php
						$args = [
							'post_type' => 'post',
							'posts_per_page' => 1,
							'meta_query' => [
								'relation' => 'AND',
								[
									'key'     => 'issue_x_post',
									'value'   => get_the_ID(),
									'compare' => 'LIKE'
								],
								[
									'key' 		=> 'featured_article',
									'value' 	=> 1
								]
							]
						];
						$featured_article_query = new WP_Query( $args );
						if( $featured_article_query->have_posts() ): ?>
							<div class="row">
								<div class="col-12 first-post-divider">
									<div></div>
								</div>
								<div class="col-6">
									<?php
									while ( $featured_article_query->have_posts() ) : $featured_article_query->the_post(); ?>
										<h3 class="grotesque font-light font-2"><?php the_title(); ?></h3>
										<?php
										if ( has_excerpt() ) { ?>
											<div class="font-1 acumin"><strong><?php the_excerpt(); ?></strong></div>
										<?php
										}
									endwhile;
									wp_reset_postdata(); ?>
								</div>
							</div>
						<?php
						endif; ?>
					</div>
				</div>

			</header>
			<?php
			if ( $articles = get_field('issue_x_post', $issue_ID) ) { ?>
				<section id="issue-articles">
					<?php
					foreach ( $articles as $article ) { ?>
						<div class="issue-article animate" data-animate="fadeInUp">
							<div class="content">
								<div class="row">
									<div class="col-5">
										<?php
										$image = '';
										$bg_color = get_field('featured_graphic_color', $article) ? get_field('featured_graphic_color', $article) : '#000000';
										if ( $graphic = get_field('featured_graphic', $article) ) {
											$image = $graphic['sizes']['medium'];
										} elseif ( has_post_thumbnail($article) ) {
											$image = get_the_post_thumbnail_url( $article, 'medium');
										} ?>
										<div class="article-image-wrapper">
											<div class="article-image-container">
												<div class="article-featured-image <?php echo get_field('featured_graphic', $article) ? 'article-graphic' : 'article-image'; ?>" style="background:<?php echo $bg_color; ?>">
													<img src="<?php echo $image; ?>"/>
												</div>
											</div>
										</div>
									</div>
									<div class="col-7 article-content">
										<p class="weight-600 font-1 grotesque-cond my-0"><?php echo get_the_date('F j, Y', $article); ?></p>
										<h2 class="grotesque-extra-cond font-5 weight-300 my-1">
											<a class="black uppercase" href="<?php echo get_permalink($article); ?>">
												<?php echo get_the_title($article); ?>
											</a>
										</h2>
									</div>
								</div>
							</div>
							<a class="article-link orange-bg flex align-center justify-center" href="<?php echo get_permalink($article); ?>">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 129 129" enable-background="new 0 0 129 129">
									<g>
										<path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
									</g>
								</svg>
							</a>
						</div>
					<?php
					} ?>
				</section>
			<?php
			}
		endwhile; // End of the loop.
		?>
		<?php past_issues($issue_ID, 'Other issues'); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
