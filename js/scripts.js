import $ from 'jquery'
import 'slick-carousel'

$(document).ready( function() {
  setTimeout( () => {
    animateDivs()
  }, 500)
})

if ( $('#home-header').length ) {
  $('#home-header .scroll-indicator').on('click', function() {
    console.log('clicked')
    $('html, body').animate({ scrollTop: $('#home-header').outerHeight() + 'px' }, 600);
  })
}

if ( $('#services-slider').length ) {
  $('.service-container').eq(0).find('.services-slide-content').slideDown()
  $('.service-container .service-label').on('click', function () {
    $('.service-container').removeClass('is-active')
    $('.service-container .services-slide-content').slideUp()
    $(this).parent().find('.services-slide-content').slideDown()
    $(this).parent().addClass('is-active')
  })
  $('#services-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    fade: true,
    appendDots: $('#services-home-nav'),
    customPaging: function( slider, i ) {
      return `
      <div class="service-label start-active">
        <span>${$('.services-slide', slider.$slides[i]).data('title')}</span>
      </div>`;
    },
  })
}

if ( $('.animate').length ) {
  $(window).on('scroll', function() {
    animateDivs();
  })
}

function animateDivs() {
  $('.animate').each( function() {
    const $animated = $(this);
    if ( !$animated.hasClass('animated') && $animated.isInViewport() ) {
      const animation = $animated.data('animate')
      $animated.addClass('animated' + ' ' + animation)
    }
  })
}

if ( $('#testimonials-slider').length ) {
  var fadeTime = $('#testimonials-slider').data('fade-time')
  var autoPlay = $('#testimonials-slider').data('autoplay')
  var slideTime = $('#testimonials-slider').data('slide-time')
  var slickSettings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: true,
    autoplay: autoPlay,
    autoplaySpeed: slideTime * 1000,
    speed: fadeTime * 1000,
    pauseOnHover: false,
    adaptiveHeight : true,
    prevArrow: '#testimonials-slider-container .prev-arrow',
    nextArrow: '#testimonials-slider-container .next-arrow'
  }

  $('#testimonials-slider').slick(slickSettings).on("beforeChange", function (event, slick, currentSlide, nextSlide) {

  })
}

if ( $('.becker-form').length ) {
  $(".becker-form input[type='text']").bind('focus', function() {
    $(this).css('background-color', 'transparent');
  });
}

if ( $('#leaders-slider').length ) {
  $('#leaders-slider .scroll-indicator').on('click', function() {
    const $content = $(this).closest('.leader-header-content')
    $($content).css('scroll-behavior', 'unset')
    setTimeout( function() {
      $($content).css('scroll-behavior', 'smooth')
    }, 800)
    $($content).animate({ scrollTop: $($content).scrollTop() + $('.leader-slide').height() }, 600);
  })
  let initSlide = 0
  if ( window.location.hash ) {
    initSlide = $(window.location.hash).index();
  }
  $('#leaders-slider').on('init', function (slick) {
    initSlideImage(initSlide)
    $('#leaders-slider-module').addClass('active')
  }).slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false,
    fade: true,
    adaptiveHeight: false,
    initialSlide: initSlide,
    // speed: 0,
    appendDots: $('#leaders-nav'),
    customPaging: function( slider, i ) {
      return `
      <div>
        <div class="inner-wrapper">
          <div>
            <h6 class="my-0 grotesque">${$('.leader-slide', slider.$slides[i]).data('name')}</h6>
            <p class="acumin my-0">${$('.leader-slide', slider.$slides[i]).data('title')}</p>
          </div>
        </div>
      </div>`;
    },
  }).on("beforeChange", function (event, slick, currentSlide, nextSlide) {
    $('.slick-slide').eq(nextSlide).find('.leader-header-content').scrollTop(0)
    $('body, html').animate({
        scrollTop: 0
    }, 1000);
    history.replaceState(null, null, '#' + $('.slick-slide').eq(nextSlide).find('.leader-slide').data('slug'));
    // if ( $('.slick-slide').eq(nextSlide).find('.becker-loader').length ) {
      initSlideImage(nextSlide)
    // } else {
    //   $('.slick-slide').find('svg path, svg polyline, svg polygon').css('opacity', 0);
    // }
  }).on("afterChange", function (event, slick, currentSlide, nextSlide) {
    if ( $('.slick-slide').eq(currentSlide).find('.becker-loader').length == 0 ) {
      $('.slick-slide').eq(currentSlide).find('svg path, svg polyline, svg polygon').each( function (i) {
        const $path = $(this)
        setTimeout ( function () {
          $path.css('opacity', 1)
        }, .15 * i )
      })
    }
  })

  function initSlideImage(slide) {
    // If image, switch out for hi-res
    if ( $('.slick-slide').eq(slide).find('.header-bg-image img.header-bg-leader-image').length && !$('.slick-slide').eq(slide).find('.hi-res-image').length ) {
      const hiResImageURL = $('.slick-slide').eq(slide).find('.leader-slide').data('image')
      $('.slick-slide').eq(slide).find('.leader-header-title').addClass('active')
      const hiResImage = document.createElement("IMG");
      hiResImage.src = hiResImageURL
      hiResImage.className = 'hi-res-image'
      $('.slick-slide').eq(slide).find('.header-bg-image').prepend(hiResImage)
      $('.slick-slide').eq(slide).find('.becker-loader').remove()
      hiResImage.onload = function () {
        $(hiResImage).addClass('is-active')
        $('.slick-slide').eq(slide).find('.header-bg-leader-image').fadeOut()
      }
    } else {
      // if SVG, get SVG and animate paths in.
      $.ajax({
        url: ajaxurl,
        method: 'post',
        type: 'json',
        data: {
          'action': 'do_ajax',
          'fn' : 'get_leader_image',
          'index' : slide
        }
      }).done(function (response) {
        if ( response ) {
          $('.slick-slide').eq(slide).find('.header-bg-image').html(response)
          $('.slick-slide').eq(slide).find('svg path, svg polyline, svg polygon').each( function (i) {
            const $path = $(this)
            setTimeout ( function () {
              $path.css('opacity', 1)
            }, .15 * i )
          })
        }
      });
    }
  }
}

$('.hamburger').on('click', () => {
  $('.hamburger').toggleClass('is-active');
  $('#site-navigation-container').toggleClass('is-active')
  setTimeout( () => {
    $('#site-navigation').toggleClass('is-active')
  }, 100)
})

$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top + ($(window).height() / 4);
  var elementBottom = elementTop + $(this).outerHeight();
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$.fn.isInFocus = function() {
  var elementTop = $(this).offset().top + ($(window).height() / 2);
  var elementBottom = elementTop + $(this).outerHeight();
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$(document).ready( function() {
  if ( $('.service').length ) {
    checkBoxes();
    $(window).on('scroll', function() {
      checkBoxes();
    });
    var hashTagActive
    $(".service-link").on("click touchstart" , function (event) {
      if(hashTagActive != this.hash) { //this will prevent if the user click several times the same link to freeze the scroll.
        event.preventDefault();
        //calculate destination place
        var dest = 0;
        const hashSlug = this.hash.replace('#', '');
        if ($(`.service-${hashSlug}`).offset().top > $(document).height() - $(window).height()) {
            dest = $(document).height() - $(window).height() + 25;
        } else {
            dest = $(`.service-${hashSlug}`).offset().top + 25;
        }
        //go to destination
        $('html,body').animate({
            scrollTop: dest
        }, 250, 'swing');
        hashTagActive = this.hash;
      }
    });
  }
})

function checkBoxes () {
  $('.service').each( function () {
    const $service = $(this)
    const serviceTitle = $service.data('title');
    if ( !$(`.service-link-${serviceTitle}`).hasClass('is-active') ) {
      if ( $service.isInFocus() ) {
        if ( window.location.hash != '#' + serviceTitle ) {
          $('.service-link').removeClass('is-active');
          history.replaceState(null, null, '#' + serviceTitle);
          $(`.service-link-${serviceTitle}`).addClass('is-active')
        }

        if ( $('.service-detail:not(.is-active)', $service).length ) {
          // If checkboxes, check boxes
          $('.detail-checkbox', $service ).each( function(i) {
            const $detail = $(this)
            if ( !$detail.hasClass('is-active') ) {
              setTimeout( function() {
                $detail.addClass('is-active')
              }, i * 300)
            }
          })
        }
      }
    }
  })
}

if ( $('.more-issues').length ) {
  var currentPage = 2;
  var loading = false;
  $('.more-issues').on('click', (e) => {
    if ( loading !== false ) {
      return false;
    }
    loading = true;
    const issue = $('.more-issues').data('issue');
    $.ajax({
      url: ajaxurl,
      method: 'post',
      type: 'json',
      data: {
        'action': 'do_ajax',
        'fn' : 'get_past_issues',
        'paged' : currentPage,
        'issue' : issue
      }
    }).done(function (response) {
      if ( response ) {
        response = $.parseJSON(response);
        $('.past-issues-row').append(response.issues)
        if ( response.more_issues ) {
          loading = false;
          currentPage++;
        } else {
          $('.more-issues').hide();
        }
        setTimeout( () => {
          animateDivs();
        }, 300)
      }
    });
    return false;
  })
}

if ( $('.becker-back-button').length ) {
  $('.becker-back-button').on('click', (e) => {
    window.history.back();
    return false;
  })
}

$(window).scroll(function() {
    if ($(this).scrollTop() > 20) { // this refers to window
        $('.page-label').addClass("hidden");
        $('.becker-back-button').addClass("hidden");
    }
    else {
      $('.page-label').removeClass("hidden");
      $('.becker-back-button').removeClass("hidden");
    }
});

$(".nav-scroll").click(function() {
    $('html,body').animate({
        scrollTop: $("#leaders-nav").offset().top},
        'slow');
});
