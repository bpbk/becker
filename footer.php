<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Becker
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer cross">
		<div class="content">
			<div class="row">
				<?php
				if ( $locations = get_field('becker_locations', 'option') ) { ?>
					<div id="becker-locations" class="col-8">
						<div class="row">
							<?php
							foreach ( $locations as $location ) { ?>
								<div class="becker-location col-4">
									<h6><?php echo $location['location']; ?></h6>
									<?php
									if ( $location['address'] ) { ?>
										<div class="becker-location-info">
											<?php echo $location['address']; ?>
											<p class="phone"><?php echo $location['phone']; ?></a>
										</div>
									<?php
									} ?>
								</div>
							<?php
							} ?>
						</div>
					</div>
				<?php
				} ?>
				<div id="footer-nav" class="col-4">
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</div>
			</div>
			<div class="row">
				<a class="email-link" href="mailto:&#98;&#98;&#117;&#99;&#104;&#97;&#110;&#97;&#110;&#64;&#98;&#101;&#99;&#107;&#101;&#114;&#109;&#101;&#100;&#105;&#97;&#46;&#110;&#101;&#116;">
					&#98;&#98;&#117;&#99;&#104;&#97;&#110;&#97;&#110;&#64;&#98;&#101;&#99;&#107;&#101;&#114;&#109;&#101;&#100;&#105;&#97;&#46;&#110;&#101;&#116;
				</a>
			</div>
		</div>
		<?php social_links(); ?>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
